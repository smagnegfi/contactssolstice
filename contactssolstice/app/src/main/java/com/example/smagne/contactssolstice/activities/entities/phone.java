package com.example.smagne.contactssolstice.activities.entities;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by smagne on 10/27/2015.
 */
public class Phone implements Parcelable {
    public String work;
    public String home;
    public String mobile;

    public Phone(){

    }

    protected Phone(Parcel in) {
        work = in.readString();
        home = in.readString();
        mobile = in.readString();
    }

    public static final Creator<Phone> CREATOR = new Creator<Phone>() {
        @Override
        public Phone createFromParcel(Parcel in) {
            return new Phone(in);
        }

        @Override
        public Phone[] newArray(int size) {
            return new Phone[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(work);
        dest.writeString(home);
        dest.writeString(mobile);
    }
}
