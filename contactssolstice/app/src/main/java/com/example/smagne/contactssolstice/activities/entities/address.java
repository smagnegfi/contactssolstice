package com.example.smagne.contactssolstice.activities.entities;

/**
 * Created by smagne on 10/27/2015.
 */
public class Address {
    public String street;
    public String city;
    public String state;
    public String country;
    public String zip;
    public double latitude;
    public double longitude;

    @Override
    public String toString(){
        return this.street + " " + this.city + " " + this.state;
    }
}
