package com.example.smagne.contactssolstice.activities.dataHandler;

import com.squareup.otto.Bus;

/**
 * Created by smagne on 11/11/2015.
 */
public class ContactBus {
    private static final Bus BUS = new Bus();

    public static Bus getInstance() {
        return BUS;
    }
}
