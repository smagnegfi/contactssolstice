package com.example.smagne.contactssolstice.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.text.format.DateFormat;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.example.smagne.contactssolstice.R;
import com.example.smagne.contactssolstice.activities.dataHandler.AsyncDetailHttp;
import com.example.smagne.contactssolstice.activities.dataHandler.ContactBus;
import com.example.smagne.contactssolstice.activities.dataHandler.ContactDetailEventResult;
import com.example.smagne.contactssolstice.activities.entities.ContactDetail;
import com.example.smagne.contactssolstice.activities.entities.Phone;
import com.squareup.otto.Subscribe;

import java.util.Date;

public class ContactDetailActivity extends ActionBarActivity {
    Phone phoneData = new Phone();
    String detailUrl;
    String name;
    String companyName;
    String birthDate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_detail);
        Intent i = this.getIntent();
        phoneData = new Phone();
        detailUrl = i.getStringExtra("DetailURL");
        name = i.getStringExtra("DetailName");
        companyName = i.getStringExtra("DetailCompany");
        birthDate = i.getStringExtra("DetailBirthDate");
        Bundle b = i.getExtras();
        if(b != null)
            phoneData = b.getParcelable("DetailPhone");

        ContactBus.getInstance().register(this);
        new AsyncDetailHttp().execute(detailUrl);
    }

    @Override
    protected void onDestroy(){
        ContactBus.getInstance().unregister(this);
        super.onDestroy();
    }

    @Subscribe
    public void onAsyncTaskResult(ContactDetailEventResult event) {
        ContactDetail result = event.getResult();
        TextView txtName = (TextView) findViewById(R.id.name);
        TextView txtCompany = (TextView) findViewById(R.id.company);
        TextView txtPhoneNumber = (TextView) findViewById(R.id.phone);
        TextView txtPhoneType = (TextView) findViewById(R.id.phoneType);
        TextView txtAddress = (TextView) findViewById(R.id.address);
        TextView txtBirthday = (TextView) findViewById(R.id.birthDay);
        TextView txtEmail = (TextView) findViewById(R.id.email);
        ImageView favoriteImage = (ImageView)findViewById(R.id.imgFavorite);
        ImageLoader imageLoader = AppController.getInstance().getImageLoader();
        NetworkImageView contactImage = (NetworkImageView) findViewById(R.id.thumbnail);

        if(result.favorite.equals("false") || result.favorite.equals("0"))
            favoriteImage.setVisibility(View.INVISIBLE);
        txtName.setText(name);
        txtCompany.setText(companyName);
        txtPhoneNumber.setText(phoneData.home);
        txtPhoneType.setText("Home");
        txtAddress.setText(result.address.toString());
        Date d = new Date(Long.parseLong(birthDate));
        txtBirthday.setText(DateFormat.format("MMMM dd, yyyy", d).toString());
        txtEmail.setText(result.email);
        contactImage.setImageUrl(result.largeImageURL, imageLoader);

    }
}
