package com.example.smagne.contactssolstice.activities.dataHandler;

import com.example.smagne.contactssolstice.activities.entities.ContactDetail;

/**
 * Created by smagne on 11/11/2015.
 */
public class ContactDetailEventResult {
    private ContactDetail _result;

    public ContactDetailEventResult(ContactDetail result){
        this._result = result;
    }

    public ContactDetail getResult(){
        return _result;
    }
}
