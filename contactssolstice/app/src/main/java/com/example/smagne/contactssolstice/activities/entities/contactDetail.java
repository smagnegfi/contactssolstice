package com.example.smagne.contactssolstice.activities.entities;

/**
 * Created by smagne on 10/27/2015.
 */
public class ContactDetail {
    public long employeeId;
    public String favorite;
    public String largeImageURL;
    public String email;
    public String website;
    public Address address;
}
