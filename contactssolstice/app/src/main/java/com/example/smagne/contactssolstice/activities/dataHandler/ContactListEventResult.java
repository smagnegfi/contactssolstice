package com.example.smagne.contactssolstice.activities.dataHandler;

import com.example.smagne.contactssolstice.activities.entities.ContactListItem;

import java.util.List;

/**
 * Created by smagne on 11/11/2015.
 */
public class ContactListEventResult {
    private List<ContactListItem> _result;

    public ContactListEventResult(List<ContactListItem> result){
        this._result = result;
    }

    public List<ContactListItem> getResult(){
        return _result;
    }
}
