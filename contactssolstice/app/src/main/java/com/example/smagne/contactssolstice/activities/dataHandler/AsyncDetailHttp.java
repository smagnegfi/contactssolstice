package com.example.smagne.contactssolstice.activities.dataHandler;

import android.app.Activity;
import android.os.AsyncTask;
import android.text.format.DateFormat;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.example.smagne.contactssolstice.R;
import com.example.smagne.contactssolstice.activities.AppController;
import com.example.smagne.contactssolstice.activities.controllers.ContactController;
import com.example.smagne.contactssolstice.activities.entities.ContactDetail;
import com.example.smagne.contactssolstice.activities.entities.Phone;

import java.util.Date;

/**
 * Created by smagne on 10/27/2015.
 */
public class AsyncDetailHttp extends AsyncTask<Object, Void, ContactDetail> {
    String detailUrl;

    @Override
    protected ContactDetail doInBackground(Object... params) {
        detailUrl = (String) params[0];
        ContactController cc = new ContactController();
        return cc.getContactDetail(detailUrl);
    }

    @Override
    protected void onPostExecute(ContactDetail result) {
        ContactBus.getInstance().post(new ContactDetailEventResult(result));
    }

    @Override
    protected void onPreExecute() {
    }

    @Override
    protected void onProgressUpdate(Void... values) {
    }
}