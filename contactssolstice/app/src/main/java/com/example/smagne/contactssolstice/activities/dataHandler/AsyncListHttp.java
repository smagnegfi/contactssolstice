package com.example.smagne.contactssolstice.activities.dataHandler;

import android.os.AsyncTask;

import com.example.smagne.contactssolstice.activities.controllers.ContactController;
import com.example.smagne.contactssolstice.activities.entities.ContactListItem;

import java.util.List;

/**
 * Created by smagne on 10/27/2015.
 */
public class AsyncListHttp extends AsyncTask<Object, Void, List<ContactListItem>> {

    @Override
    protected List<ContactListItem> doInBackground(Object... params) {
        ContactController cc = new ContactController();
        return cc.getAllContacts();
    }

    @Override
    protected void onPostExecute(List<ContactListItem> result) {
        ContactBus.getInstance().post(new ContactListEventResult(result));
    }

    @Override
    protected void onPreExecute() {
    }

    @Override
    protected void onProgressUpdate(Void... values) {
    }
}