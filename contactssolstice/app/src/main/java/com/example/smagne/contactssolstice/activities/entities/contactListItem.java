package com.example.smagne.contactssolstice.activities.entities;

/**
 * Created by smagne on 10/27/2015.
 */
public class ContactListItem {
    public long employeeId;
    public String name;
    public String company;
    public String detailsURL;
    public String smallImageURL;
    public String birthdate;
    public Phone phone;

    @Override
    public String toString(){
        return this.name;
    }
}
