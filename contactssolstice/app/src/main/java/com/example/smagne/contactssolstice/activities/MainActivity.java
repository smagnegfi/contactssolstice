package com.example.smagne.contactssolstice.activities;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.example.smagne.contactssolstice.R;
import com.example.smagne.contactssolstice.activities.customHelpers.CustomListAdapter;
import com.example.smagne.contactssolstice.activities.dataHandler.AsyncListHttp;
import com.example.smagne.contactssolstice.activities.dataHandler.ContactBus;
import com.example.smagne.contactssolstice.activities.dataHandler.ContactListEventResult;
import com.example.smagne.contactssolstice.activities.entities.ContactListItem;
import com.example.smagne.contactssolstice.activities.entities.Phone;
import com.squareup.otto.Subscribe;

import java.util.List;


public class MainActivity extends ActionBarActivity {
    List<ContactListItem> listItems;
    CustomListAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ContactBus.getInstance().register(this);
        new AsyncListHttp().execute(this);
    }

    @Override
    protected void onDestroy() {
        ContactBus.getInstance().unregister(this);
        super.onDestroy();
    }

    @Subscribe
    public void onAsyncTaskResult(ContactListEventResult event) {
        listItems = event.getResult();
        adapter = new CustomListAdapter(this, listItems);

        ListView lv = (ListView) findViewById(R.id.lvContacts);
        lv.setAdapter(adapter);
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                ContactListItem r = (ContactListItem) adapter.getItem(position);
                contactDetail(r.detailsURL, r.name, r.company, r.birthdate, r.phone);
            }
        });
    }

    public void contactDetail(String detailUrl, String name, String companyName, String birthDate, Phone phone){
        Intent i = new Intent(getApplicationContext(), ContactDetailActivity.class);
        i.putExtra("DetailURL", detailUrl);
        i.putExtra("DetailName", name);
        i.putExtra("DetailCompany", companyName);
        i.putExtra("DetailBirthDate", birthDate);
        Bundle b = new Bundle();
        b.putParcelable("DetailPhone", phone);
        i.putExtras(b);
        startActivityForResult(i, 100);
    }
}
